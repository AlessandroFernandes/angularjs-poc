angular.module('minhasDiretivas', ['MeusServicos'])
	.directive('meuPainel', function() {

		var ddo = {};

		ddo.restrict = "AE";
        ddo.transclude = true;


		ddo.scope = {
            titulo: '@'
        };

        ddo.templateUrl = 'js/directives/meu-painel.html';

		return ddo;
	})
    .directive('minhaFoto', function() {

        var ddo = {};

        ddo.restrict = "AE";

        ddo.scope = {
            titulo: '@',
            url: '@'
        };

        ddo.template = '<img class="img-responsive center-block" src="{{url}}" alt="{{titulo}}">';           
        
        return ddo;
    })
    .directive('botaoRemover', function(){
        return{
            restrict : 'E',
            scope: {
                titulo: '@',
                acao: '&'
            },
            template: `<button class="btn btn-danger btn-block" ng-click="acao(foto)">{{titulo}}</button>`
        };
    })
    .directive('gatilhoFocus', function(){
        return{
            restrict: 'A',
          /*  scope: {
                focado : '='
            },
            link : function(scope, element){
                scope.$watch('focado',function(){
                    if(scope.focado){
                        element[0].focus();
                        scope.focado = false;
                    }
                })
            }*/
            link : function(scope, element){
                scope.$on('focado', () => element[0].focus())
            }
        }   
    })
    .directive('listaTitulo',function(){
        return{
            restrict: 'E',
            template: `<ul><li ng-repeat='item in itens'>{{item}}</li></ul>`,
            controller: function($scope, servicoFotos){
                servicoFotos.query(fotos => $scope.itens = fotos.map(foto => foto.titulo))
                    }
                }
    })
