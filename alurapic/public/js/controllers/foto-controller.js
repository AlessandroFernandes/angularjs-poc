angular.module('alurapic')
	.controller('FotoController', function($scope, $routeParams, servicoFotos, servicoCentral) {

		$scope.foto = {};
		$scope.mensagem = '';
	
		if($routeParams.fotoId){
			servicoFotos.get({fotoId : $routeParams.fotoId},
				dados => $scope.foto = dados,
				error => console.log(error)
			)
		/*	$http.get('/v1/fotos/' + $routeParams.fotoId)
				 .success(dados => $scope.foto = dados)
				 .error(erro => console.log(erro))*/
		}
		$scope.submeter = function(){
			if($scope.formulario.$valid){
				servicoCentral.cadastrar($scope.foto)
							  .then(dados =>{
									$scope.mensagem = dados.mensagem;
									if(dados.inclusao) $scope.foto = {};
							})
							  .catch(()=> $scope.mensagem = dados.mensagem)
			}
		}
		/*$scope.submeter = function() {
				if($routeParams.fotoId){
					servicoFotos.editar({fotoId : $scope.foto._id}, $scope.foto,
						()=> $scope.mensagem = "Modificação concluída",
						error => { console.log(error),
								   $scope.mensagem = "Erro na modificação"}
					)
						$http.put('/v1/fotos/'+ $scope.foto._id, $scope.foto)
						 .success(()=> $scope.mensagem = 'Modificação concluída')
						 .error(()=> $scope.mensagem = 'Erro na modificação')
				}else{
					if ($scope.formulario.$valid) {
		
						servicoFotos.save($scope.foto, 
									()=> { $scope.foto = {},
									$scope.mensagem = "Foto cadastrada com sucesso"},
									error => {console.log(error),
									$scope.mensagem = "Não foi possível cadastrar a foto"}
						)
						$http.post('/v1/fotos', $scope.foto)
						.success(function() {
							$scope.foto = {};
							$scope.mensagem = 'Foto cadastrada com sucesso';
						})
						.error(function(erro) {
							console.log(erro);
							$scope.mensagem = 'Não foi possível cadastrar a foto';
						})
					}
				}
		};*/
	});