angular.module('alurapic').controller('FotosController', function($scope, servicoFotos) {
	
	$scope.fotos = [];
	$scope.filtro = '';
	$scope.message = '';

	servicoFotos.query(
		fotos => $scope.fotos = fotos,
		error => console.log(error)
	);

	/*
	$http.get('/v1/fotos')
			.success(function(retorno) {
				$scope.fotos = retorno; // não precisa fazer retorno.data
			})
			.error(function(erro) {
				console.log(erro);
	});
	*/
	$scope.remover = function(foto){
		servicoFotos.delete({fotoId: foto._id},
			()=>{
				$scope.fotos.splice($scope.fotos.indexOf(foto), 1);
				$scope.message = `A foto ${foto.titulo} foi removida com sucesso!`;
			},
			()=> $scope.message = `A foto ${foto.titulo} não foi removida!`
		)
		
		/*$http.delete('/v1/fotos/' + foto._id)
			 .success(()=>{
				$scope.fotos.splice($scope.fotos.indexOf(), 1);
				$scope.message = `A foto ${foto.titulo} foi removida com sucesso!`;
			 })
			 .error(error => {
				 console.log(error);
				 $scope.message = `A foto ${foto.titulo} não foi removida!`
				});*/
	}

});