angular.module('alurapic').controller('CategoriaController', function($scope, servicoGrupo){

    $scope.categoria = [];

    servicoGrupo.query(
        itens => $scope.categoria = itens,
        error => console.log(error)
    )
    /*$http.get('/v1/grupos')
         .success(itens => $scope.categoria = itens)
         .error(error => console.log(error))*/
})