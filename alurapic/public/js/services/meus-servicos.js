angular.module('MeusServicos',['ngResource'])
    .factory('servicoFotos', function($resource){
       return $resource('/v1/fotos/:fotoId', null, 
           {editar: { method : 'PUT'}}
           )
    })
    .factory('servicoGrupo', function($resource){
        return $resource('/v1/grupos')
    })
    .factory('servicoCentral', function(servicoFotos, $q, $rootScope){
        let servico = {};
        servico.cadastrar =  function(foto){
            return $q(function(resolve, reject){
                if(foto._id){
                    servicoFotos.editar({fotoId: foto._id}, foto, function(){
                        $rootScope.$broadcast('focado');
                        resolve({
                            mensagem: `Foto ${foto.titulo} editada`,
                            inclusao: false
                        })
                    },function(error){
                        console.log(error);
                        reject({
                            mensagem: `Não foi possível editar a foto ${foto.titulo}`
                        })
                    })
                }else{
                    servicoFotos.save(foto, function(){
                        $rootScope.$broadcast('focado');
                        resolve({
                            mensagem: `Foto ${foto.titulo} incluida com sucesso`,
                            inclusao: true
                        })
                    }, function(error){
                        console.log(error);
                        reject({
                            mensagem: `Não foi possível adicionar a foto!`
                        })
                    })
                }
            })
        }
        return servico;
    })